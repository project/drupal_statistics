Drupal Statistics

This module consist of two blocks:

1.Node Statistics block - 
  Place on node pages it will give you count of the number of 
  times that particular node is viewed by authenticated users.
  If not node page it will return not a node page.

2.User Statistics block - 
  Place anywhere it will return following user information:
  - Read history
  - Number of nodes visited by the user
  - Join date
  - Days registered
  - Last login time
  - Days since last login
